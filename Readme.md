# TodoApp

    Author: Girish Kumar
    Version: 1.0
    Release Date: 17-mar-2018

Requirnments:
-------------
    1. Python 3
    2. PyQt5

How to install:
---------------
    1. Open terminal
	2. Enter following commands in the terminal
   		
		$ cd path/to/downloaded/folder
    	$ chmod +x install.sh
		$ ./install.sh
How to use:
-----------
	1. Open terminal and enter this command
	
		$ todo
	
	2. A window will pop-up. Now you are good to go :)
Next Version:
-------------
    1. Desktop Entry
