#!/bin/bash
echo Installing . . .
echo Creating bin folder in home directory
mkdir -p ~/bin
echo Creating .todo folder in home directory
mkdir -p ~/.todo
echo Copying files
cp src/task_icon.png ~/.todo/
echo task_icon.png copied to .todo folder
echo Seeting up todoApp . . .
comnd="src/setup ~"
eval $comnd
mv src/todoApp ~/.todo/
echo todoApp copied to .todo folder
cp src/todo ~/bin/
echo todo  file copied in bin folder
echo Copy completed
echo Giving ~/bin/todo file execute permission
chmod +x ~/bin/todo
echo Giving todoApp file execute permission
chmod +x ~/.todo/todoApp
echo Installation completed
echo Thank you for choosing TodoApp
